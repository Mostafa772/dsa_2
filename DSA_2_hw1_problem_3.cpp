//
// Created by mostafa on 19.09.2021.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <queue>

std::vector<int> Parser () {

    std::string str = " ";
    int node;

    std::cin.ignore();

    std::getline(std::cin, str);

    std::vector<int> nod;

    std::string nodes = " ";

    for (size_t i = 0; i < str.length(); ++i) {

        if(nodes.back()>= '0' && nodes.back() <= '9' && str[i] >= '0' && str[i] <= '9') {

            nodes += str[i];
            if ((i + 1) < str.length() && str[i + 1] >= '0' && str[i + 1] <= '9') {

                continue;
            }

            node = std::stoi(nodes);
            std::cout << "node: " << node << std::endl;
            nod.push_back(node);
            nodes = "";

        } else if (str[i] >= '0' && str[i] <= '9') {

            nodes = str[i];

            if ((i + 1) < str.length() && str[i + 1] >= '0' && str[i + 1] <= '9') {

                continue;

            }
            node = std::stoi(nodes);

            nod.push_back(node);

            nodes = "";
        }
    }

    return nod;
}

std::vector<std::vector<size_t>> AdjacencyMatrix (const std::vector<int>& nodes, size_t nodes_num) {

    std::vector<std::vector<size_t>> adjacency_matrix(nodes_num);

    for(size_t i = 0; i < nodes.size(); i += 2 ) {

        adjacency_matrix[nodes[i] - 1].push_back(nodes[i + 1]);

    }

    return adjacency_matrix;
}

size_t even_cycle = 0;

size_t BFS (std::vector<std::vector<size_t>>& adjacency_matrix, size_t& num_nodes, size_t vertix) {

    std::vector<bool> visited(num_nodes);
    std::queue<size_t> q;
    std::vector<size_t> distance(num_nodes + 1);

    distance[vertix] = 0;
    q.push(vertix);
    visited[vertix] = true;

    while (!q.empty()) {

        size_t u = q.front();
        q.pop();
        size_t even_distance;

        for (auto& x : adjacency_matrix[u - 1]) {

            if(visited[x]) {

                even_distance = abs(distance[x] - distance[u]) + 1;

                if (even_distance % 2 == 0) {
                    even_cycle = even_cycle + 1;
                }
            }
            if (!visited[x]) {
                visited[x] = true;
                distance[x] = distance[u] + 1;
                q.push(x);
            }
        }
    }

    return even_cycle;
}


int main () {

    size_t num_of_nodes;
    size_t num_of_edges;

    std::cin >> num_of_nodes;
    std::cin >> num_of_edges;

    std::vector<int> inp = Parser();

    std::vector<std::vector<size_t>> adjacency_matrix = AdjacencyMatrix(inp, num_of_nodes);

    std::cout << BFS(adjacency_matrix, num_of_nodes, 2);

    return 0;
}
