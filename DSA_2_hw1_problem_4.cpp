//
// Created by mostafa on 19.09.2021.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

std::vector<size_t> PowersOfSmallerNumbers (size_t const number) {

    std::vector<size_t> adjacency_matrix;

    for (size_t i = 1; i*i < number; ++i) {
        adjacency_matrix.push_back(i*i);
    }

    return adjacency_matrix;
}

size_t Solve(size_t number) {
    
    std::vector<size_t> power_series = PowersOfSmallerNumbers(number);
    std::vector<size_t> lengths;
    size_t number2;

    for (int j = power_series.size() - 1; j >= 0; --j) {

        size_t length = 0;
        number2 = number;

        for (int i = j; i >= 0; --i) {

            if ((number2 / power_series[i]) > 0) {

                length += (number2 / power_series[i]);
            }

            number2 = number2 % power_series[i];
        }

        if (length > 0) {
            lengths.push_back(length);
        }

    }
    auto min = std::min_element(lengths.begin(),lengths.end());
    return *min;
}

int main () {
    size_t number;
    scanf("n = %lu", &number);
    std::cout << Solve(number) << std::endl;

    return 0;
}

