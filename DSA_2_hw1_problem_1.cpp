//
// Created by mostafa on 14.09.2021.
//
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <queue>

std::vector<int> Parser () {

    std::string str = " ";
    int node;

    std::cin.ignore();

    std::getline(std::cin, str);
    std::vector<int> nod;

    std::string nodes = " ";
    for (size_t i = 0; i < str.length(); ++i) {

        if(nodes.back()>= '0' && nodes.back() <= '9' && str[i] >= '0' && str[i] <= '9') {

            nodes += str[i];

            if ((i + 1) < str.length() && str[i + 1] >= '0' && str[i + 1] <= '9') {

                continue;

            }

            node = std::stoi(nodes);

            std::cout << "node: " << node << std::endl;

            nod.push_back(node);

            nodes = "";

        } else if (str[i] >= '0' && str[i] <= '9') {

            nodes = str[i];

            if ((i + 1) < str.length() && str[i + 1] >= '0' && str[i + 1] <= '9') {

                continue;
            }

            node = std::stoi(nodes);

            nod.push_back(node);

            nodes = "";
        }
    }

    return nod;
}

std::vector<std::vector<size_t>> AdjacencyMatrix (const std::vector<int>& nodes, size_t nodes_num) {


    std::vector<std::vector<size_t>> adjacency_matrix(nodes_num);

    for(size_t i = 0; i < nodes.size(); i += 2 ) {
        /// (1, 2) (1, 3) (3, 4) (2, 4) (2, 3)
        adjacency_matrix[nodes[i] - 1].push_back(nodes[i + 1]);
        adjacency_matrix[nodes[i + 1] - 1].push_back(nodes[i]);
    }

    return adjacency_matrix;
}

size_t BFS (std::vector<std::vector<size_t>>& adjacency_matrix, size_t& num_nodes, size_t vertix) {

    std::vector<bool> visited(num_nodes);
    std::queue<size_t> q;
    std::vector<size_t> distance(num_nodes + 1);

    distance[vertix] = 0;
    q.push(vertix);
    visited[vertix] = true;

    while (!q.empty()) {

        size_t u = q.front();
        q.pop();

        for (auto& x : adjacency_matrix[u - 1]) {

            if (!visited[x]) {
                visited[x] = true;
                distance[x] = distance[u] + 1;
                q.push(x);
            }
        }

    }
    size_t max = *std::max_element(distance.begin(), distance.end());

    return max;
}

size_t Solve(std::vector<std::vector<size_t>>& adjacency_matrix, size_t& num_nodes) {
    size_t maximum = 0;

    for (size_t i = 1; i <= num_nodes; ++i) {
        size_t distance = BFS(adjacency_matrix, num_nodes, i);

        if(distance > maximum) {

            maximum = distance;

        }
    }

    return maximum;
}

int main () {

    size_t num_of_nodes;
    size_t num_of_edges;

    std::cin >> num_of_nodes;
    std::cin >> num_of_edges;

    std::vector<int> inp = Parser();

    std::vector<std::vector<size_t>> adjacency_matrix = AdjacencyMatrix(inp, num_of_nodes);

    std::cout << Solve(adjacency_matrix,num_of_nodes);

    return 0;
}
