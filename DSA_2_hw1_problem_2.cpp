//
// Created by mostafa on 14.09.2021.
//
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>


std::vector<size_t> Parser (size_t num_rows,size_t num_of_columns) {
    std::vector<size_t> nod;

    size_t node = 0;
    if(num_rows == 1 && num_of_columns ==1) {
        std::cin >>node;

        if (node == 1) {
            std::cout << "NO";
        }else{
            std::cout << "YES";
        }
    }
    size_t size = num_rows * num_of_columns;

    for (size_t i = 0; i < size; ++i) {
        std::cin >> node;
        nod.push_back(node);
    }

    return nod;
}

std::vector<std::vector<size_t>> AdjacencyMatrix (const std::vector<size_t>& nodes, size_t nodes_num, size_t num_columns) {

    size_t size = nodes_num * num_columns;
    std::vector<std::vector<size_t>> adjacency_matrix(size);

    for (size_t j = 0; j < size; ++j)
    {
        if (nodes[j] == 0) {

            for (size_t i = 0; i < nodes.size(); ++i) {

                if(nodes[i]==0 && i != j) {

                    int x = ((j / num_columns) - (i / num_columns));

                    bool adjacent = abs(x) < 2;

                    int a = static_cast<int>(j) % num_columns;
                    int b = static_cast<int>(i) % num_columns;

                    if (abs(b - a) < 2 && adjacent) {
                        adjacency_matrix[j].push_back(i);
                    }
                }
            }
        }
    }
    return adjacency_matrix;
}

bool BFS (std::vector<std::vector<size_t>>& adjacency_matrix, size_t num_nodes, size_t num_columns) {

    if ( adjacency_matrix[0].empty()) {
        return false;
    }

    std::vector<bool> visited(num_nodes * num_columns);

    std::queue<size_t> q;

    q.push(0);

    visited[0] = true;

    while (!q.empty()) {

        size_t u = q.front();
        q.pop();

        for (auto& x : adjacency_matrix[u]) {

            if (!visited[x]) {

                visited[x] = true;
                q.push(x);
            }
        }

    }
    return visited[num_nodes*num_columns-1];
}

int main () {

    size_t num_rows;
    size_t num_colums;

    std::cin >> num_rows;
    std::cin >> num_colums;

    if (num_colums == 1 && num_rows == 1){
        Parser(num_rows,num_colums);
        return 0;
    }

    std::vector<size_t> nodes = Parser(num_rows, num_colums);

    std::vector<std::vector<size_t>> adjacency_matrix = AdjacencyMatrix(nodes, num_rows,num_colums);

    if(BFS(adjacency_matrix, num_rows, num_colums)) {
        std::cout << "YES";
    } else {
        std::cout << "NO";
    }
    return 0;
}
